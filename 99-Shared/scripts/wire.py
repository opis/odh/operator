from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model.persist import WidgetColorService

line_style = 1  # dashed

pvSevStr  = "N/A"
pvStatStr = "N/A"
pvIntStr  = "null"
pvTime    = ""

try:
    pvSev     = PVUtil.getSeverity(pvs[0])
    pvSevStr  = PVUtil.getSeverityString(pvs[0])
    pvStatStr = PVUtil.getStatus(pvs[0])

    pvInt    = PVUtil.getLong(pvs[0])
    pvIntStr = PVUtil.getString(pvs[0])

    pvTime   = PVUtil.getTimeString(pvs[0])


    if pvSev == 3:  # INVALID
        raise RuntimeError

    if pvInt == True and pvSev == 0:
        line_style = 0  # solid
        line_color = WidgetColorService.getColor("GREEN-BORDER")
        tooltip    = "Healthy connection"
    else:
        line_color = WidgetColorService.getColor("ERROR")
        tooltip    = "NO connection"
except:
    line_color = WidgetColorService.getColor("DISCONNECTED")
    tooltip    = "Unknown connection status"

widget.setPropertyValue('line_color', line_color)
widget.setPropertyValue('line_style', line_style)
widget.setPropertyValue('tooltip',    tooltip + """
{}
{}, {} - {}, {}""".format(str(pvs[0]), pvIntStr, pvSevStr, pvStatStr, pvTime))
